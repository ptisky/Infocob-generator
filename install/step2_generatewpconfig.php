<?php
session_start();

/*** Pointe sur le projet généré ***/
chdir('../../' . $_SESSION['directory']);

/*** Copie and rename wp-config ***/
copy('wp-config-sample.php', 'wp-config.php');

/*** Recupere le contenu de wp-config ***/
$str = file_get_contents('wp-config.php');

/*** Remplace la base de donnée et les KEYS du wp-config ***/
/*********************************************************************/
$str = str_replace($_SESSION['prefix_from'], $_SESSION['prefix_to'], $str);
$str = str_replace($_SESSION['txtDebug_from'], $_SESSION['txtDebug_to'], $str);

$str = str_replace($_SESSION['dbname_from'], $_SESSION['dbname_to'], $str);
$str = str_replace($_SESSION['dbuser_from'], $_SESSION['dbuser_to'], $str);
$str = str_replace($_SESSION['dbpass_from'], $_SESSION['dbpass_to'], $str);
$str = str_replace($_SESSION['dbhost_from'], $_SESSION['dbhost_to'], $str);

$str = str_replace($_SESSION['keyTemplateDel'], $_SESSION['keyTemplateTo'], $str);
$str = str_replace("define( 'SECURE_AUTH_KEY',  'mettez une phrase unique ici' );", "", $str);
$str = str_replace("define( 'LOGGED_IN_KEY',    'mettez une phrase unique ici' );", "", $str);
$str = str_replace("define( 'NONCE_KEY',        'mettez une phrase unique ici' );", "", $str);
$str = str_replace("define( 'AUTH_SALT',        'mettez une phrase unique ici' );", "", $str);
$str = str_replace("define( 'SECURE_AUTH_SALT', 'mettez une phrase unique ici' );", "", $str);
$str = str_replace("define( 'LOGGED_IN_SALT',   'mettez une phrase unique ici' );", "", $str);
$str = str_replace("define( 'NONCE_SALT',       'mettez une phrase unique ici' );", "", $str);
/*********************************************************************/

/*** Réécris le wp-config ***/
file_put_contents('wp-config.php', $str);

if (file_exists("./wp-config.php")) {
    echo "true";
} else {
    header('HTTP/1.1 500 Internal Server');
    header('Content-Type: application/json; charset=UTF-8');
    session_destroy();
    die(json_encode(array('message' => 'wp-config est introuvable à cet emplacement : ' . getcwd(), 'code' => 1)));
}