<?php
session_start();

/**
 * Permet de supprimer un dossier avec ses fichiers
 * @param $dirname
 * @return bool
 */
function delete_files($dirname)
{
    if (is_dir($dirname))
        $dir_handle = opendir($dirname);
    if (!$dir_handle)
        return false;
    while ($file = readdir($dir_handle)) {
        if ($file != "." && $file != "..") {
            if (!is_dir($dirname . "/" . $file))
                unlink($dirname . "/" . $file);
            else
                delete_files($dirname . '/' . $file);
        }
    }
    closedir($dir_handle);
    rmdir($dirname);
    return true;
}

/*** Pointe sur le projet généré ***/
chdir('../../' . $_SESSION['directory']);

/*** Wordpress fonctions ***/
require_once('wp-load.php');
require_once('wp-admin/includes/upgrade.php');
require_once('wp-includes/wp-db.php');

/*** Pointe sur le dossier themes ***/
chdir("wp-content/themes");

/*** Theme Parent ***/
exec("git clone https://wordpressexporter:dbMgbbap8LVAYUkQr8aq@gitlab.com/MInfocob/theme-wordpress-infocob-web"); #PARENT THEME
rename("theme-wordpress-infocob-web", "infocobweb"); #rename le theme

/*** Theme Enfant ***/
if ($_SESSION['demoSelector'] === "demo1") {
    exec("git clone https://wordpressexporter:Dj9BJe99P6ubzo7YxySY@gitlab.com/infocob-web/wordpress/themes/theme-wordpress-standard-batiment"); #Récupere le theme sur git
    rename("theme-wordpress-standard-batiment", "infocobweb-child"); #rename le theme

} elseif ($_SESSION['demoSelector'] === "demo2") {
    exec("git clone https://wordpressexporter:jJGsh3UZQjHyg9TsLwd_@gitlab.com/infocob-web/wordpress/themes/theme-wordpress-standard-incendie"); #Récupere le theme sur git
    rename("theme-wordpress-standard-incendie", "infocobweb-child"); #rename le theme

} elseif ($_SESSION['demoSelector'] === "demo3") {
    exec("git clone https://wordpressexporter:pY_NkwNftA7Rb_U9Tsbu@gitlab.com/infocob-web/wordpress/themes/theme-wordpress-standard-restauration"); #Récupere le theme sur git
    rename("theme-wordpress-standard-restauration", "infocobweb-child"); #rename le theme

} elseif ($_SESSION['demoSelector'] === "demo4") {
    exec("git clone https://wordpressexporter:yvqmyLHN3e_8yEEMPL1A@gitlab.com/infocob-web/wordpress/themes/theme-wordpress-standard-ville"); #Récupere le theme sur git
    rename("theme-wordpress-standard-ville", "infocobweb-child"); #rename le theme
}

/*** Change de theme ***/
switch_theme('infocobweb-child');

/*** Supprime les thèmes inutile mais en garde un au cas ou ***/
delete_files('twentynineteen');
delete_files('twentytwenty');

/*** Tests ***/
if (file_exists("./infocobweb/functions.php") && file_exists("./infocobweb-child/functions.php")) {
    echo "true";
} else {
    header('HTTP/1.1 500 Internal Server');
    header('Content-Type: application/json; charset=UTF-8');
    session_destroy();
    die(json_encode(array('message' => 'Thèmes Infocob introuvable à cet emplacement : ' . getcwd(), 'code' => 1)));
}