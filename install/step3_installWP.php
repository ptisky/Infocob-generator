<?php
session_start();

/*** Pointe sur le projet généré ***/
chdir('../../' . $_SESSION['directory']);

/*** Variable globale d'instalation de wordpress initialisé ***/
define('WP_INSTALLING', true);

/*** Wordpress fonctions ***/
require_once('wp-load.php');
require_once('wp-admin/includes/upgrade.php');
require_once('wp-includes/wp-db.php');


try {
    /*** Lance l'instalation de Wordpress ***/
    wp_install($_SESSION['sitename'], $_SESSION['siteuser'], $_SESSION['mailuser'], 1, '', $_SESSION['pdwuser']);
    echo "true";
} catch (\Exception $ex) {
    header('HTTP/1.1 500 Internal Server');
    header('Content-Type: application/json; charset=UTF-8');
    session_destroy();
    die(json_encode(array('message' => $ex->getMessage(), 'code' => 1)));
}


