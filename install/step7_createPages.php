<?php
session_start();

/*** Pointe sur le projet généré ***/
chdir('../../' . $_SESSION['directory']);

/*** Wordpress fonctions ***/
require_once('./wp-load.php');
require_once('./wp-admin/includes/upgrade.php');
require_once('./wp-includes/wp-db.php');

/*** Pointe sur le projet Generation de wordpress ***/
chdir("../infocobWP.local");

/*** Créations des pages par default ***/
/*********************************************************************/
/*** On vérifie que data.ini existe ***/
if (file_exists('data.ini')) {

    /*** Nous analysons le fichier et obtenons le tableau ***/
    $file = parse_ini_file('data.ini');

    /*** On pointe sur le projet ***/
    chdir("../" . $_SESSION['directory']);

    /*** Nous vérifions si nous avons au moins une page ***/
    if (count($file['posts']) >= 1) {

        foreach ($file['posts'] as $post) {

            /*** On obtient la ligne de configuration de la page ***/
            $pre_config_post = explode("-", $post);
            $post = array();

            foreach ($pre_config_post as $config_post) {

                /*** On récupère le titre de la page ***/
                if (preg_match('#title::#', $config_post) == 1) {
                    $post['title'] = str_replace('title::', '', $config_post);
                }

                /*** Nous récupérons le statut (publier, brouillon, etc...) ***/
                if (preg_match('#status::#', $config_post) == 1) {
                    $post['status'] = str_replace('status::', '', $config_post);
                }

                /*** Sur récupérer le type de publication (types de publication, de page ou de publication personnalisée ...) ***/
                if (preg_match('#type::#', $config_post) == 1) {
                    $post['type'] = str_replace('type::', '', $config_post);
                }

                /*** Nous récupérons le contenu ***/
                if (preg_match('#content::#', $config_post) == 1) {
                    $post['content'] = str_replace('content::', '', $config_post);
                }

                /*** On récupère le Slug ***/
                if (preg_match('#slug::#', $config_post) == 1) {
                    $post['slug'] = str_replace('slug::', '', $config_post);
                }

                /*** On récupère le titre du parent ***/
                if (preg_match('#parent::#', $config_post) == 1) {
                    $post['parent'] = str_replace('parent::', '', $config_post);
                }

            }

            if (isset($post['title']) && !empty($post['title'])) {

                $parent = get_page_by_title(trim($post['parent']));
                $parent = $parent ? $parent->ID : 0;

                /*** On créé la page ***/
                $args = array(
                    'post_title' => trim($post['title']),
                    'post_name' => $post['slug'],
                    'post_content' => trim($post['content']),
                    'post_status' => $post['status'],
                    'post_type' => $post['type'],
                    'post_parent' => $parent,
                    'post_author' => 1,
                    'post_date' => date('Y-m-d H:i:s'),
                    'post_date_gmt' => gmdate('Y-m-d H:i:s'),
                    'comment_status' => 'closed',
                    'ping_status' => 'closed'
                );
                wp_insert_post($args);

            }

        }
    }
}
/*********************************************************************/

/*** Création des menus ***/
$menunameMain = 'Main Menu';
$menunameFooter = 'Menu footer';

$menu_exists = wp_get_nav_menu_object($menunameMain);
$menufooter_exists = wp_get_nav_menu_object($menunameFooter);

/*** Menu MAIN ***/
/*********************************************************************/
if (!$menu_exists) {
    $menu_id = wp_create_nav_menu($menunameMain);


    /*** Configure les liens par défaut et les ajoutes au menu ***/
    wp_update_nav_menu_item($menu_id, 0, array(
        'menu-item-title' => __('Accueil'),
        'menu-item-classes' => 'accueil',
        'menu-item-url' => home_url('/'),
        'menu-item-status' => 'publish'));

    wp_update_nav_menu_item($menu_id, 0, array(
        'menu-item-title' => __('Actualités'),
        'menu-item-classes' => 'actualites',
        'menu-item-url' => home_url('/actualites'),
        'menu-item-status' => 'publish'));

    wp_update_nav_menu_item($menu_id, 0, array(
        'menu-item-title' => __('Contact'),
        'menu-item-classes' => 'contact',
        'menu-item-url' => home_url('/contact'),
        'menu-item-status' => 'publish'));

    wp_update_nav_menu_item($menu_id, 0, array(
        'menu-item-title' => __('Notre société'),
        'menu-item-classes' => 'notre-societe',
        'menu-item-url' => home_url('/notre-societe'),
        'menu-item-status' => 'publish'));

    wp_update_nav_menu_item($menu_id, 0, array(
        'menu-item-title' => __('Page d’exemple'),
        'menu-item-classes' => 'page-d-exemple',
        'menu-item-url' => home_url('/page-d-exemple'),
        'menu-item-status' => 'publish'));
}
/*********************************************************************/

/*** Menu Footer ***/
/*********************************************************************/
if (!$menufooter_exists) {
    $menu_id = wp_create_nav_menu($menunameFooter);

    /*** Configure les liens par défaut et les ajoutes au menu ***/
    wp_update_nav_menu_item($menu_id, 0, array(
        'menu-item-title' => __('Accueil'),
        'menu-item-classes' => 'accueil',
        'menu-item-url' => home_url('/'),
        'menu-item-status' => 'publish'));

    wp_update_nav_menu_item($menu_id, 0, array(
        'menu-item-title' => __('Actualités'),
        'menu-item-classes' => 'actualites',
        'menu-item-url' => home_url('/actualites'),
        'menu-item-status' => 'publish'));

    wp_update_nav_menu_item($menu_id, 0, array(
        'menu-item-title' => __('Contact'),
        'menu-item-classes' => 'contact',
        'menu-item-url' => home_url('/contact'),
        'menu-item-status' => 'publish'));

    wp_update_nav_menu_item($menu_id, 0, array(
        'menu-item-title' => __('Notre société'),
        'menu-item-classes' => 'notre-societe',
        'menu-item-url' => home_url('/notre-societe'),
        'menu-item-status' => 'publish'));

    wp_update_nav_menu_item($menu_id, 0, array(
        'menu-item-title' => __('Page d’exemple'),
        'menu-item-classes' => 'page-d-exemple',
        'menu-item-url' => home_url('/page-d-exemple'),
        'menu-item-status' => 'publish'));
}
/*********************************************************************/

/*** Pointe le menu Main sur l'ancre associé ***/
if (!has_nav_menu("main-menu")) {
    $locations = get_theme_mod('nav_menu_locations');
    $locations["main-menu"] = $menu_id;
    set_theme_mod('nav_menu_locations', $locations);
}

/*** Pointe le menu Footer sur l'ancre associé ***/
if (!has_nav_menu("footer-menu-infocobweb")) {
    $locations = get_theme_mod('nav_menu_locations');
    $locations["footer-menu-infocobweb"] = $menu_id;
    set_theme_mod('nav_menu_locations', $locations);
}


/*** Associe la page d'accueil en première page ***/
$about = get_page_by_title('Accueil');
update_option('page_on_front', $about->ID);
update_option('show_on_front', 'page');

/*** Associe la page Actualité en page de post ***/
$blog = get_page_by_title('Actualités');
update_option('page_for_posts', $blog->ID);

/*** On regénére les permaliens ***/
update_option('permalink_structure', '/%postname%/');

/*** Tests ***/
if (get_page_by_title('Accueil') && get_page_by_title('Actualités')) {
    echo "true";
} else {
    header('HTTP/1.1 500 Internal Server');
    header('Content-Type: application/json; charset=UTF-8');
    session_destroy();
    die(json_encode(array('message' => 'Erreur lors de la création des pages', 'code' => 1)));
}
